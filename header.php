

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/spectre.min.css">
    <link rel="stylesheet" href="css/spectre-exp.min.css">
    <link rel="stylesheet" href="css/spectre-icons.min.css">
	<style type="text/css">
		header {
			max-width:960px;
			margin:0 auto;
			margin-top:40px;
		}
		footer {
			max-width:960px;
			margin:0 auto;
			margin-top:40px;	
		}
		div.container {
			margin-top:40px;
		}
		h1 {
			font-size:24px;
		}
		h2 {
			font-size:20px;
		}
	</style>
  </head>
  <body>
	<header class="navbar">
	  <section class="navbar-section">
		<a href="about-us.php" class="btn btn-link">About Us</a>
			<a href="locations.php" class="btn btn-link">Locations</a>
		<a href="menu.php" class="btn btn-link">Menu</a>
	  </section>
	  <section class="navbar-center">
		<a href="index.php"><img src="images/logo.png" style="max-width:128px;"></a>
	  </section>
	  <section class="navbar-section">
		<a href="#" class="btn btn-link">Twitter</a>
		<a href="#" class="btn btn-link">Instagram</a>
	  </section>
	</header>	
