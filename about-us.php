	
<?php
include("header.php");
?>
	<div class="container">
	  <div class="columns">
		<div class="column col-9 centered">
			<h1 class="text-center"> ABOUT US </h1>
			<img src="images/burnt-toast-donut.jpg" class="centered">
			<p style="padding-top:20px">
				The Magic Donut Shop was born out of a love for experimentation. As leaders in the confectionery game, the goal is to combine conceptual thinking with high-quality ingredients and products, in an effort to make something that tastes and looks, unlike anything you’ve ever seen and had before. Toronto born and globally grown; Sweet Jesus is the little ice cream shop that wouldn't quit. 
			</p>
			<p>
				Our name was created from the popular phrase that people use as an expression of enjoyment, surprise or disbelief. Our aim is not to offer commentary on anyone’s religion or belief systems, Our own organization is made up of amazing people that represent a wide range of cultural and religious beliefs.
			</p>
		</div>
		</div>
	  </div>
	</div> <!-- // container -->
  
	<?php
include("footer.php");
?>
