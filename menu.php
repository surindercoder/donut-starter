
<?php
require("db_credentials.php");
?>

<?php
include("header.php");
?>
<?php


$connection = connect();
$results = getProducts();
    ?>

	<div class="container">
		<div class="columns">
			<div class="column col-9 centered">
				<h1 class="text-center"> Menu </h1>
				<p class="text-center"> Pick a donut! </p>
				
				<div class="columns col-gapless"> 
				 <?php while ($products = mysqli_fetch_assoc($results)) { ?>
					<div class="column col-6 text-center">
				 <img src="<?php echo $products["image"]?>">
				
				 
						<h2> <?php echo $products["name"] ?>  </h2>
						<p>
							<?php echo "$". $products["price"] ?> 
						</p>
						<button class="btn btn-primary"> Buy </button>
						   
					</div>
					 <?php } ?>
				
				</div>
			</div>
		</div>
	</div> <!-- // container -->
  
<?php
include("footer.php");
?>
