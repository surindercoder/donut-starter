<?php
	define("DBHOST", "localhost");
	define("DBUSER", "root");
	define("DBPASS", "");
	define("DBNAME", "donut");
	
	function connect() {
		// 1. Create a database connection
		$connection = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME);

		// show an error message if PHP cannot connect to the database
		if (mysqli_connect_errno())
		{
		  echo "Failed to connect to MySQL: " . mysqli_connect_error();
		  exit();
		}
		
		return $connection;
	}
	
	function getLocations() {
		
		global $connection;
		
		$sql = "SELECT * FROM location";

		$results = mysqli_query($connection, $sql);

		if ($results == FALSE) {
		  echo "Database query failed. <br/>";
		  echo "SQL command: " . $sql;
		  exit();
		}
		
		return $results;
		
	}
	
	function getProducts(){
		global $connection;
		
		 $query = "SELECT * FROM product";

      $results = mysqli_query($connection, $query);

      // 4 - Check if there are errors in your query
      if ($results == FALSE) {
        echo "Database query failed. <br/>";
        echo "SQL command: " . $query;
        exit();
      }
	  return $results;
	}
	
	
	
	
?>