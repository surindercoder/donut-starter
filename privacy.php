			<?php
include("header.php");
?>
			<p>
			At The Magic Donut Shop, we strongly believe in protecting the privacy of the personal information that we obtain for business purposes about our customers. To that end, we have established our Privacy Policy. In addition, we have developed Privacy Principles to guide us in the interpretation and application of our Privacy Policy. We also have Website Privacy Guidelines to protect the privacy of visitors to our website on The Magic Donut Shop.ca.
			If you have any questions, comments or concerns regarding privacy, please contact the The Magic Donut Shop Privacy Manager.
			</p>

			<h2>Privacy Policy</h2>
			<p>
			At The Magic Donut Shop, we strongly believe in protecting the privacy of the personal information that we obtain for business purposes about our customers, employees, franchisees, suppliers and other entities. Also, we value the trust that is placed on us to responsibly manage the personal information that we obtain.
			</p>

			<p>
			To that end, we have established Privacy Guidelines to assist us in protecting the privacy of employee and non-employee personal information. We have also established a set of Employee Privacy Principles and Non-Employee Privacy Principles. These Principles provide guidance to us with respect to our application of these Guidelines and our treatment of personal information.
			</p>

			<p>
			These Guidelines and the accompanying Principles are designed to assist us in fairly and effectively managing personal information with regard to collection, use, disclosure, retention, accuracy and security. In that regard, all of our employees who handle personal information are required to comply with these Guidelines and our Principles.
			</p>

			<p>
			We are committed to following these Guidelines and our Principles. We are further committed to complying with any privacy laws that may apply to the personal information that we may obtain.
			</p>
			<p>
			We encourage our customers, the general public and others to learn more about our Privacy Guidelines by visiting our website - The Magic Donut Shop.ca; speak with a The Magic Donut Shop manager; or, contact the The Magic Donut Shop person responsible for privacy issues. We encourage our employees to learn more about our Privacy Guidelines by visiting McWeb or speaking with a manager.
			From time to time these Guidelines and our Principles may be amended to address the changing needs of our business and/or changes in applicable privacy laws.
			</p>
<?php
include("footer.php");
?>